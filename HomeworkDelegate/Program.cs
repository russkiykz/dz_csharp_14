﻿using System;

namespace HomeworkDelegate
{
    class Program
    {
        
        static void Main(string[] args)
        {
            var button = new Button
            {
                Border = '*',
                Height = 5,
                Width = 12,
                TextInButton = "Button",
                FillingColor = ConsoleColor.White,
                BorderColor = ConsoleColor.Red
            };

            button.Act += new ActDelegate<string>(Action);
            
            button.MappingButton();
            Console.WriteLine("\nНажмите любую клавишу...");
            Console.ReadLine();
            button.SomeMethod();
        }

        private static void Action(params string[] values)
        {
            Console.WriteLine($"{values[0]} и {values[1]}");
        }


    }
}
