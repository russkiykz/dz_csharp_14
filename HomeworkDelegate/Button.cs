﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkDelegate
{
    public delegate void ActDelegate<T>(params T[] values);
    public class Button
    {
        public char Border { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string TextInButton { get; set; }
        public ConsoleColor FillingColor { get; set; }
        public ConsoleColor BorderColor { get; set; }
        public ActDelegate<string> Act { get; set; }
        
        public void MappingButton()
        {
            if (Width < TextInButton.Length || Height < 3) return;
            int[,] array = new int[Height, Width];
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    if (i > 0 && j > 0 && i < Height - 1 && j < Width - 1)
                    {
                        Console.BackgroundColor = FillingColor;
                        Console.Write(" ");
                        Console.ResetColor();
                    }
                    else if (i == Height / 2)
                    {
                        Console.ForegroundColor = BorderColor;
                        Console.Write(Border);
                        Console.BackgroundColor = FillingColor;
                        Console.Write($" {TextInButton.PadLeft(Width/2).PadRight(Width-3)}");
                        Console.ResetColor();
                        Console.ForegroundColor = BorderColor;
                        Console.Write(Border);
                        Console.ResetColor();
                        break;
                    }
                    else
                    {
                        Console.ForegroundColor = BorderColor;
                        Console.Write(Border);
                        Console.ResetColor();
                    }
                }
                Console.WriteLine();
            }
        }

        public void SomeMethod()
        {
            Act?.Invoke("Что-то", "Еще что-то");
        }
    }
}
